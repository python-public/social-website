# Приложение для ведения блога
## Django Projects

<ul><b>Социальный веб-сайт</b> для создания закладок и обмена изображениями
<li>Реализованна аутентификация при помощи Django authentication framework</li>
<li>Расширение пользовательской модели</li>
<li>Использование Django messages framework</li>
<li>Использование django-extensions для запуска сервера разработки через HTTPS</li>
<li>Ресайз изображений с помощью easy-thumbnails</li>
<li>Реализованно отношение many-to-many в моделях</li>
<li>Асинхронные HTTP запросы через Fetch API и Django</li>
<li>Реализованна бесконечная прокрутка страниц</li>
<li>Отслеживание действий пользователя и вывод из в ленту событий</li>
<li>Оптимизация запросов</li>
<li>Использованы сигналы Django</li>
<li>Добавлен django-debug-toolbar</li>
<li>Подсчет просмотра изображений с помощью Redis</li>
<li>Создан рейтинг изображений с помощью Redis</li>
</ul>

## Запуск приложения
<ol>
<li>pip install -r requirements.txt</li>
<li>docker pull redis</li>
<li>docker run -it --rm --name redis -p 6379:6379 redis</li>
<li>docker exec -it redis sh</li>
<li>redis-cli</li>
<li>python manage.py makemigrations</li>
<li>python manage.py migrate</li>
<li>python manage.py runserver_plus --cert-file cert.crt</li>
</ol>